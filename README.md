 # Docker UniFi-Controller

> **This Project is deprecated. Look at my new [Docker Swarm Project](https://codeberg.org/chrishue/Docker-Swarm-UniFi-Network-Application-stack)**
 
Docker-Compose stack for UniFi-Controller

<!-- TOC -->
- [1. Usage](#1-usage)
  - [1.1. Pqarameters](#11-pqarameters)
    - [1.1.1. Ports (Required)](#111-ports-required)
    - [1.1.2. Environments (Optional)](#112-environments-optional)
    - [1.1.3. Traefik Labels - Reverse Proxy](#113-traefik-labels---reverse-proxy)
- [2. Application Setup](#2-application-setup)
  - [2.1. Unifi Controller](#21-unifi-controller)
    - [2.1.1. Adopt Devices](#211-adopt-devices)
    - [2.1.2. Network Setup](#212-network-setup)
<!-- /TOC -->

## 1. Usage

### 1.1. Pqarameters
#### 1.1.1. Ports (Required)

To avoid a likely already reserved Port issue, add oder append this to `docker-compose.override.yml`.

```yaml
  unifi-controller:
    ports:
      # Unifi web admin port
      - 8443:8443
      # Unifi STUN port
      - 3478:3478/udp
      # AP Discovery
      - 10001:10001/udp
      # Device communication
      - 8080:8080
      # Device Discovery on L2 network
      - 1900:1900/udp
      # Guest Portal
      - 8843:8843
      # Guest Portal (Http only)
      - 8880:8880
      # For mobile throughput test
      - 6789:6789
      # Remote syslog port
      - 5514:5514/udp
```
> As Alternative create your own docker-compose prod file and use it like this:
>   `docker-compose -f docker-compose.prod.yml up -d`

#### 1.1.2. Environments (Optional)

If you have to, add this to `docker-compose.override.yml` for the corresponding service as well.

Example for unifi-controller service:
```yaml
  environment:
    - TZ=Europe/Zurich
    - PUID=1000
    - PGID=1000
    - MEM_LIMIT=1024
    - MEM_STARTUP=1024
```

#### 1.1.3. Traefik Labels - Reverse Proxy

Minimum adaptations:

Add this to `docker-compose.override.yml`.
* Replace `portainer.yourdomain.com`.
* Replace `yourmiddleware@file`.

```yml
networks:
  traefik-net:
    external: true

services:
  unifi-controller:
    networks:
      - traefik-net
      - stack
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-net"
      - "traefik.http.routers.unifi-controller.rule=Host(`unifi.yourdomain.com`)"
      - 'traefik.http.routers.unifi-controller.entrypoints=websecure'
      - "traefik.http.routers.unifi-controller.tls=true"
      - "traefik.http.routers.unifi-controller.tls.certresolver=letsencrypt"
      - "traefik.http.routers.unifi-controller.middlewares=yourmiddleware@file"
      - "traefik.http.services.unifi-controller.loadbalancer.server.scheme=https"
      - "traefik.http.services.unifi-controller.loadbalancer.server.port=8443"
```

## 2. Application Setup

### 2.1. Unifi Controller

#### 2.1.1. Adopt Devices

1. In order to adopt a device create two DNS Entries in your local network. Both entries must point to your controller instance.
- `unifi.local` the default inform-endpoint domain for initial adoption
- `your.localdomain` the new inform-endpoint

2. Go to Settings > System Settings > Controller Configuration and check the checkbox "Override inform host" with your new inform-endpoint Domain: `your.localdomain`

The alternative way and origin source of this Image: https://docs.linuxserver.io/images/docker-unifi-controller

#### 2.1.2. Network Setup

1. Let's rename the default Network `Homenet` or something.
2. Setup Gateway IP und Subnet with your attached Network and disable Auto Scale Network
3. Setup DHCP as a DHCP Relay

**More Networks**

> **Attention: VLAN ID 1 is not available**

- Create a VLAN-only Network and setup your DHCP Server IP.
- Create a WiFi-Network with this Network.